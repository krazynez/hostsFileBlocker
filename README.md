Host File Blocker

Author: Brennen Murphy

Date: 11/27/2019

Contact Me: BrennenMurph@pm.me

<p style="text-decoration: underline;">Or Join Our</p>


<a href="https://discord.gg/pAh6Cpv"><img src="https://pmcvariety.files.wordpress.com/2018/05/discord-logo.jpg?w=1000&h=563&crop=1" alt="Discord" title="Discord - Linux Guide To The Galaxy" width="100px" height="50px" style="border-radius: 5px;"></a>


##########################################

**Special Thanks to Steven Black's hosts file on github and Tom from Switched To Linux for providing the
bulk of the work with the premade hosts files.**

##########################################


 -----------------------------------------------------------------------------------------------------------------------------

**To run: curl https://raw.githubusercontent.com/krazynez/hostsFileBlocker/master/runme.sh -o runme.sh && sudo bash runme.sh**
  
 **OR: git clone https://github.com/krazynez/hostsFileBlocker && cd hostsFileBlocker && sudo bash runme.sh**
 
 -----------------------------------------------------------------------------------------------------------------------------
