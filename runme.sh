#!/bin/bash

# Author: Brennen Murphy
# Version: 0.2.1b
# Date: 11/27/2019
# Contact Me: BrennenMurph@pm.me
# Discord Server: https://discord.gg/pAh6Cpv







# check for root user
clear
if ! [[ "$EUID" -eq 0 ]]; then
  echo ""
  echo "Need to be run as root!"
  sleep 2
  exit 0;
fi

if ! [[ -f "/usr/bin/wget" && -f "/usr/bin/unzip" ]]; then
	echo ""
	echo "You need to make sure wget and unzip are installed."
	echo ""
	exit 0;
else
	echo "Dependencies met"

fi
# customHost function


customHost() {
  #gets Steven Black hosts
  wget https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts
  # deletes lines 1 thru 28
  sed -i '1,28d' hosts

  # Replaces all the 0.0.0.0's with 127.0.0.1
  sed -i -e 's/0.0.0.0/127.0.0.1/g' hosts



  #gets STL hosts
  wget https://switchedtolinux.com/wp-content/uploads/2017/12/stlblock.txt.zip

  unzip stlblock.txt.zip

  cat stlblock.txt >> hosts


  clear
  echo ""

  read -p "ECPI Student? Don't need Live Chat on campus webpage? y/n: " ecpi

  if [[ "$ecpi" == "y" || "$ecpi" == "Y" ]]; then
    echo "127.0.0.1 https://secure.livechatinc.com
  127.0.0.1 secure.livechatinc.com
  127.0.0.1 cdn.livechatinc.com" >> hosts
  fi
  clear
  echo "Please Wait..."
  echo "backing up original hosts file in /etc/hosts as /etc/hosts.bak"
  cp /etc/hosts /etc/hosts.bak

  # changes original host file if all 0.0.0.0 preset to 127.0.0.1
  sed -i -e 's/0.0.0.0/127.0.0.1/g' /etc/hosts
  cat hosts >> /etc/hosts
  rm hosts && rm stlblock*
  echo ""
  echo "Done."

  # Exit function
  x


} # END OF CUSTOMHOST FUNCTION






# Revert function

revert() {

  cp /etc/hosts.bak /etc/hosts
  # Exit function
  x

} # end of revert function



updateHost() {

clear
echo ""
echo "####################################################################"
echo "#                                                                  #"
echo "# WARNING! IF YOU HAVE ADDED YOUR OWN PERSONAL LIST TO /etc/hosts  #"
echo "# THEN PLEASE COPY THE LIST YOU CREATED TO /etc/hosts.bak OR THEY  #"
echo "# WILL BE LOST AS THE UPDATE WILL TAKE THE BACKUP AND REBUILD FROM #"
echo "# THAT. YOU HAVE BEEN WARNED!                                      #"
echo "#                                                                  #"
echo "####################################################################"
echo ""
read -p "Press enter to continue..."


clear
echo ""
read -p "Are you sure your want to update? y/n: " userInput


if [[ "$userInput" == "y" || "$userInput" == "Y" ]]; then
  cp /etc/hosts.bak /etc/host
  customHost
else
  # Exit function
  x
fi




}


















# exit function
x() {
echo ""
echo "Goodbye!"
exit 0;

}

ask() {

clear
echo ""
echo "##############################################################"
echo "#                                                            #"
echo "#  1.) Use Custom Hosts File                                 #"
echo "#                                                            #"
echo "#  2.) Revert back to Original Host File                     #"
echo "#                                                            #"
echo "#  3.) Update Custom Host                                    #"
echo "#                                                            #"
echo "#  4.) Exit                                                  #"
echo "#                                                            #"
echo "##############################################################"
echo ""
read -p "Choice: " userChoice

case $userChoice in
   1) customHost
   ;;
   2) revert
   ;;
   3) updateHost
   ;;
   4) x
   ;;
   *) echo "You didn't enter 1-4!"
   sleep 2
   ask
   ;;
esac
}




echo "##########################################################################"
echo "# This will update your host file to removed unwanted ads, keystokes,    #"
echo "# porn, fakenews, and much much more!                                    #"
echo "# THIS WILL UPDATE YOUR /etc/hosts FILE!!!! It shouldn't effect anything #"
echo "# just add to the end of the file                                        #"
echo "# I would like to thank Steven Black for his hosts file on               #"
echo "# github @ https://github.com/StevenBlack/hosts and                      #"
echo "# Tom from https://switchedtolinux.com for his hosts file.               #"
echo "#  ----------------                                                      #"
echo "#  - **REMINDER** -                                                      #"
echo "#  ----------------                                                      #"
echo "# I have not gone thru both hosts files to check for redundancy.         #"
echo "##########################################################################"
echo ""
read -p "Press enter to continue..."
# Ask function
ask
